# Init you nodeJs app
```
cd workspace
npm install
```

# Start your service and Traefik

`docker-compose up -d`

# Check Traefik dashboard
Go to http://localhost:8080

# Start to play

Check your app on http://localhost

Scale your service

`docker-compose scale js-poc=10`

Check Traefik dashboard updates

Refresh http://localhost

# Stop to play

`docker-compose down`