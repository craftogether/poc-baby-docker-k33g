const express = require("express");
const bodyParser = require("body-parser");
const generate = require('project-name-generator');

let port = process.env.PORT || 9090;

let app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

let machineName = generate({ words: 3, number: true }).dashed

app.disable('etag');

app.get('/', (req, res) => {
  res.send(`
    <!doctype html>
    <html>
      <head>
        <meta charset="utf-8">
      </head>
      <body>
        <h1>👋 Hello 🌍, I am ${machineName}</h1>
      </body>
    </html>
  `)
})

app.listen(port)
console.log(`🌍 ${machineName} is started - listening on`, port)