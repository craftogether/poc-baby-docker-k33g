FROM ubuntu:latest
LABEL maintainer="@k33g_org"
 
RUN apt-get update && \
    apt-get install -y apt-utils && \
    apt-get install -y curl && \
    apt-get install -y nano && \
    apt-get install -y gnupg2 && \
    curl -sL https://deb.nodesource.com/setup_11.x | bash  && \
    apt-get install -y nodejs  && \
    apt-get clean

WORKDIR /workspace
